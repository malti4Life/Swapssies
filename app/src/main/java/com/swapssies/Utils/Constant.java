package com.swapssies.Utils;

/**
 * Created by MaltiDevnani on 3/1/2017.
 */

public class Constant {
    public static final String KEY_STATUS = "res_code";
    public static final String KEY_MESSAGE = "res_message";
    public static final String KEY_DATA = "res_object";
}
