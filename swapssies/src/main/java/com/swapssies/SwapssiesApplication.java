package com.swapssies;


import android.app.Application;

import com.swapssies.Webservices.RestClient;

import static okhttp3.internal.Internal.instance;

public class SwapssiesApplication extends Application{
    private static SwapssiesApplication instance;
    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;



        new RestClient();
    }
}
