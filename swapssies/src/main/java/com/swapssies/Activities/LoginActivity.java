package com.swapssies.Activities;

import com.swapssies.R;

/**
 * Created by MaltiDevnani on 3/1/2017.
 */

public class LoginActivity extends BaseActivity {
    @Override
    protected void initView() {

    }

    @Override
    protected void initToolBar() {

    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_login;
    }
}
