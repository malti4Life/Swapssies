package com.swapssies.Activities;

import android.content.Intent;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.swapssies.R;
import com.swapssies.utils.Logger;
import com.swapssies.utils.Utils;

import okhttp3.internal.Util;

public class SplashActivity extends BaseActivity {

    @Override
    protected void initView() {
        new SplashCountDown(2000,1000).start();
    }

    @Override
    protected void initToolBar() {

    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_splash;
    }

private class SplashCountDown extends CountDownTimer{

    /**
     * @param millisInFuture    The number of millis in the future from the call
     *                          to {@link #start()} until the countdown is done and {@link #onFinish()}
     *                          is called.
     * @param countDownInterval The interval along the way to receive
     *                          {@link #onTick(long)} callbacks.
     */
    public SplashCountDown(long millisInFuture, long countDownInterval) {
        super(millisInFuture, countDownInterval);
    }

    @Override
    public void onTick(long millisUntilFinished) {

    }

    @Override
    public void onFinish() {
       if(Utils.isConnectedToInternet(SplashActivity.this)){
           Intent intent;
           intent = new Intent(SplashActivity.this,LoginActivity.class);
           navigateToNextActivity(intent,true);
       }else{
           Logger.toast(SplashActivity.this,getString(R.string.not_internet));
           finish();
       }
    }
}
}
